---
title: Como contribuir
toc: true
sidebar:
    left:
        sticky: true
widgets:
-
    type: toc
    position: left
categories:
#- [Orçamento, Painel Orçamentário]
#- [Orçamento, PLOA automatizada]
#- [Preditivo, Levantamento Epidemiológico]
#- [Preditivo, Previsão de compras]
- [Preditivo, Tecnologias Aplicadas]
#- [Farmácia Popular, Monitoramento]
#- [Farmácia Popular, Notificação]
#- [Farmácia Popular, Ressarcimento]
#- [Capacitação, Cursos]
#- [Capacitação, Eventos]
#- [Capacitação, Workshops]
---

Para inserir novas páginas à wiki, é preciso ter conhecimentos prévios em [Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet), e [Node scripts](https://docs.npmjs.com/misc/scripts). Este repositório contém o seguintes scripts para execução:
 - `node run server`: executa o hexo server para testes locais.
 - `node run generate`: executa o hexo generate para popular o diretório `public` que será utilizado em produção.
 - `node run new <params>`: executa o hexo new para criar novos conteúdos para a wiki, a documentação do comando está [aqui](https://hexo.io/docs/commands#new).
 - `node run deploy`: executa o hexo deploy. Este comando é utilizado apenas pelo gitlab CI para pipelines de CI/DC.

## Novas páginas

Para adicionar novas páginas a wiki, crie um diretório novo em `source` e dentro do diretório crie um arquivo `index.md`. O comando `npm run new page <page-title>` fará exatamente isso, sendo que `<page_title>` é o nome da página que você deseja criar. Por padrão, os nomes das páginas da wiki são em inglês. Por exemplo, a página [Sobre](/msdaf/wiki/about) possui a url `about` e a página [Processos](/msdaf/wiki/processes) possui a url `processes`.

Ao criar uma página, observe a seção no topo iniciada por `---`, por padrão, as páginas que possuem links diretos na barra superior, possuem apenas um componente lateral: Table of Contents. que é adicionado da seguinte forma:
```yml
title: Sobre o Projeto
date: 2019-03-11 12:09:42
toc: true

sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left
```

O widget `toc`(table of contents) é adicionado a esquerda em uma sidebar fixa (sticky). Dessa forma, mantemos o padrão visual da wiki para facilitar a busca por conteúdos correlacionados e para facilitar o compartilhamento de links para regiões diretas das publicações.

Para cada página definida, é importante listar as categorias e subcategrias pertencentes a ela. Observe a lista de categorias no código fonte [desta página](https://gitlab.com/gaesi/msdaf/wiki/tree/master/source/_posts/contributing.md) como exemplo.

## Novos posts

Para criar um post, basta utilizar o comando `npm run new <post-title>`. Por padrão o hexo irá criar uma publicação, ou seja, um arquivo `<post-title>.md` dentro do diretório `sources/_posts`. Para as publicações, temos que o widget básico é o mesmo das páginas internas, mas podemos adicionar tags e categorias a uma publicação da seguinte forma.

```yml
title: Publicação exemplo
date: 2019-03-11 12:09:42
toc: true

sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left
    -
        type: tags
        position: left
categories:
- [Sobre, Equipe]
- [Sobre, Projeto]
tags:
- tag1
- tag2
```
Desse modo é possível adicionar uma publicação na categoria desejada e tags para facilitar busca por termos específicos.
