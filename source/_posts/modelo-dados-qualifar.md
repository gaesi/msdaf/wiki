---
title: Modelo de dados QUALIFAR
date: 2019-04-04 16:14:20
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

tags:
categories:
# - [Orçamento, Painel Orçamentário]
# - [Orçamento, PLOA automatizada]
# - [Preditivo, Levantamento Epidemiológico]
- [Preditivo, Previsão de compras]
# - [Farmácia Popular, Monitoramento]
# - [Farmácia Popular, Notificação]
# - [Farmácia Popular, Ressarcimento]
# - [Capacitação, Cursos]
# - [Capacitação, Eventos]
# - [Capacitação, Workshops]
---
# Modelo de Entidades e Relacionamentos QUALIFAR

Os dados planilhados relativos aos municípios habilitados no QUALIFAR e seus respectivos repasses, foram carregados em uma aplicação com saída em formato JSON para consulta externa.
As multiplas planilhas disponibilizadas forma padronizadas para importação segundo o modelo abaixo.

![MER das planilhas de dados QUALIFAR](../images/post-mer-qualifar/mer-qualifar.jpeg "MER das planilhas de dados QUALIFAR")

De acordo com o modelo estipulado, os dados de contato, habilitação, repasses e demais informações estarão disponíveis em serviço REST online.
