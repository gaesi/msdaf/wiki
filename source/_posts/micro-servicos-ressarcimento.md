---
title: Automações no setor de Ressarcimento - Farmácia Popular
date: 2019-07-08 16:37:34
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

tags:
categories:
# - [Orçamento, Painel Orçamentário]
# - [Orçamento, PLOA automatizada]
# - [Preditivo, Levantamento Epidemiológico]
# - [Preditivo, Previsão de compras]
# - [Farmácia Popular, Monitoramento]
# - [Farmácia Popular, Notificação]
- [Farmácia Popular, Ressarcimento]
# - [Capacitação, Cursos]
# - [Capacitação, Eventos]
# - [Capacitação, Workshops]
---

Para a proposta de automação de procedimentos executados pelo setor de Farmácia Popular do DAF no que tange o ressarcimento ao erário publico oriundo de irregularidades identificadas pela auditoria nos procedimentos executados pelas farmácias, foram identificados três fases em que a aplicação de automação na execução dos procedimento agilizará o trabalho executado, bem como reduzirá a probabilidades de erros e inconsistências.

## Fase de atualização de valores de débito

Nesta fase os profissionais do setor extraiam dados do SNA (Sistema Nacional de Auditoria do SUS) contendo os valores a serem ressarcidos ao ministério pelas farmácias. Após a extração do arquivo no formato CSV, alteravam o mesmo tendo que, por exemplo, remover as aspas de todos os campos que já vinham dos arquivos, ou seja, caso houvesse 30 linhas com os valores extraídos deveriam editar 60 campos somente para essa alteração. Então com o arquivo já alterado convertiam no formato TXT, formato este aceito pelo site do TCU o qual realiza o recálculo e atualização dos valores, utilizando uma ferramenta descontinuada, ou seja, não havia atualizações nem mesmo contrato de manutenção da mesma. Com esses procedimentos realizados importavam o arquivo para site do TCU preenchiam os campos faltantes como nome da farmácia e obtinham os valores desejados.

Com a analise e estudo dos procedimento apresentados pelos profissionais do setor de Farmácia Popular do DAF e expostos acima foi desenvolvido um ferramenta implementada de acordo com as diretrizes do DATASUS, ou seja, utilizando o conceito de micro serviços, e com ela um interface de interação para que os profissionais pudessem testa-la e homologa-las na realização das atividades. Esta ferramenta é utilizada de modo a reduzir as atividades realizadas pelos profissionais resultando a agilidade do processo, então basta o mesmo extrair do portal do SNA de forma indicada no manual disponibilizado na ferramenta e incluir no campo indicado o arquivo CSV. A ferramenta retornara o arquivo TXT já proto para ser incluído no site do TCU e contendo o nome da razão social da empresa (farmácia), com isso os mesmo não precisam editar nada no arquivo CSV e nem mesmo no TXT, as conversões e tratamento de dados são realizados pelo micro serviço contido na ferramenta.

Com o apresentado conclui-se que a utilização desta ferramenta contribui na agilidade do serviço executado pelos profissionais do setor, agilidade essa impossível de ser obtida de forma manual, corroborando assim pela utilização da mesma.


## Fase de consulta do Sistema SISGRU

Nesta fase do processo e ressarcimento os profissionais do setor de Farmácia Popular do DAF devem consultar se há algum pagamento desta natureza durante o período entre  auditoria e a cobrança para que possa deduzir do valor a ser cobrado, ou seja, caso o contribuinte devedor tenha pago alguma quantia referente a esta auditoria o valor pago deve ser descontado da cobrança a ser feita. Neste procedimento o profissional acessa o sistema SISGRU que contem dados de GRUs (Guia de Recolhimento da União), documento pelo qual são realizado os pagamentos, vinculado a este contribuinte no período solicitado. Este sistema possibilita que sejam consultados os pagamento realizado a cada 12 meses ou seja caso o período seja maior deve se realizar diversas consultas.

Nesta etapa foi identificada que o SISGRU possuía uma ferramenta de comunicação externa, web-service, que possibilitava a consulta dos dados por outros sistema, fornecendo a autenticação necessária para a mesma. Com isso, após estudo e analise deste recurso, foi desenvolvida uma ferramenta para realizar as consultas necessária para esse processo independente do período, ou seja, os profissionais não precisariam realizar diversas consultas para obterem os dados desejados, apenas uma consulta será realizada para as obtenção.

Com isso obter-se a otimização o tempo levado para em cada consulta levando assim uma economicidade de tempo de trabalho neste procedimento.


## Fase de Emissão da GRU

Nesta fase observou-se que para a emissão do documento de cobrança GRU os profissionais devem acessar o site do tesouro nacional preencher com os dados do setor o qual vincula a GRU ao órgão bem como os dados do contribuinte e de cobrança. Para que não seja necessário o acesso a outro site foi desenvolvida uma funcionalidade que apenas recebe os dados necessários para geração da GRU e fornece realiza a geração, facilitando o trabalho dos profissionais, para futuras aplicações também foi desenvolvido a ferramenta de geração de GRU como micro-serviços passando apenas os dados necessário podendo em uma aplicação futura utilizar para geração em massa.

Com a funcionalidade, será possível obter uma produtividade ímpar nos trabalhos executados, não sendo necessário o acesso de vários sites e sistema para realizar cada atividade, limitando-se apenas um ambiente.


A utilização das ferramentas desenvolvidas, análise e comparação dos procedimento apresentados acima foi obtido um aproveitamento de tempo considerável dando a possibilidade da realização de mas tarefas no mesmo espaço de tempo, respeitando assim o Princípio da Eficiência incluso das diretrizes do serviço público pela reforma administrativa realizada em 1998 e como citado por *Alexandre de Morais*, hoje Ministro no STF, em seu livro **Direito Constitucional** de 1999, que diz:

> *“aquele que impõe à Administração Pública direta e indireta a seus agentes a persuasão do bem comum, por meio do exercício de suas competências de forma imparcial, neutra, transparente, participativa, eficaz, sem burocracia e sempre em busca da qualidade, primando pela adoção dos critérios legais e morais necessários para a melhor utilização possível dos recursos públicos, de maneira a evitar-se desperdícios e garantir-se maior rentabilidade social. Note-se que não se trata da consagração da tecnocracia, muito pelo contrário, o princípio da eficiência dirige-se para a razão e fim maior do Estado, a prestação dos serviços essenciais à população, visando à adoção de todos os meios legais e morais possíveis para a satisfação do bem comum”*  

**MORAES**, *Alexandre de.* **Direito Constitucional** *. 7. Ed. 	São Paulo: Atlas, 1999, p. 294.*


Com isso conclui-se que a aplicação dos micros-serviços desenvolvidos é essencial para o bom aproveitamento do tempo gasto com as realizações dos trabalho pelos profissionais do setor de Farmácia Popular.
