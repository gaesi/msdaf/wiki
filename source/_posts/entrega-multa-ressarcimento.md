---
title: Processo de multa e ressarcimento - Especificação Técnica
date: 2019-08-02 11:57:01

toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left
    -
        type: tag
        position: left
tags:
 - entrega

categories:
# - [Orçamento, Painel Orçamentário]
# - [Orçamento, PLOA automatizada]
# - [Preditivo, Levantamento Epidemiológico]
# - [Preditivo, Previsão de compras]
- [Farmácia Popular, Monitoramento]
# - [Farmácia Popular, Notificação]
- [Farmácia Popular, Ressarcimento]
# - [Capacitação, Cursos]
# - [Capacitação, Eventos]
# - [Capacitação, Workshops]
---

## Objetivo
Desenvolver documentação do subprocesso de multa e ressarcimento, identificando pontos de melhoria e possíveis automações com uso de recursos tecnológicos como suporte.

## Visão
O subprocesso de multa e ressarcimento está contido no programa Farmácia Popular, responsável por identificar, notificar e em último caso multar ou ressarcir farmácias registradas. Devido à existência de certo volume passivo de indícios em aberto para análise, o processo de Multa e Ressarcimento foi identificado como prioritário para avaliação e melhoria.

## Escopo
 - Identificação de validação dos procedimentos atuais relacionados ao processo de multa e ressarcimento
 - Documentação dos procedimentos padrões do processo
 - Identificação de gargalos, mudanças ou oportinudades de melhoria do processo
 - Desenvolvimento de protótipos para avaliação das melhorias propostas
 - Documentação técnica e funcional dos protótipos desenvilvidos

## Processo

![Modelo Multa Ressarcimento v3.png](https://gaesi.gitlab.io/msdaf/wiki/images/entrega-multa-ressarcimento/Modelo_MultaRessarcimento_v2.png "Modelo Multa Ressarcimento")

#### Objetivo do sub-processo: garantir o ressarcimento de valores pagos pelo erário à farmácias participantes do Programa Farmácia Popular do Brasil que cometeram atos contrários ao exercício do programa.

O processo de ressarcimento se inicia com um documento chamado “Relatório de Auditoria” (RA), construído pelo Denasus com um conjunto de informações relativas à auditoria realizada em uma determinada farmácia participante dentro de um determinado período de tempo, farmácia essa que, após solicitação de documentos anterior, não conseguiu justificar determinadas transações que fogem à portaria regulamentadora do programa.

Esse RA pode ser motivado por duas razões principais:
 1. Por sinalização de indício de fraude advinda da equipe de monitoramento, indícios esses materializados após a farmácia não conseguir comprovar documentalmente a correta dispensação de medicamentos em um conjunto de transações suspeitas;
 2. Por motivação de outros órgãos de controle e auditoria externos ao MS;

O procedimento de instauração de averiguação pelo Denasus tem origem em após o Denasus enviar um ofício para uma farmácia participante (e seus responsáveis legais no intervalo da auditoria) solicitando a comprovação documental de um número de transações com indícios de fraude/irregularidade ao programa, e esse conjunto de pessoas físicas e jurídicas não retornar comprovantes de que essas transações foram realizadas dentro das premissas do programa. Ao final desse procedimento de instauração, constrói-se o RA.

Posterior à não completa resposta a esses questionamentos, inicia-se o processo de ressarcimento e/ou de descredenciamento da farmácia participante. Nessa fase, o fluxo se desdobra entre as ações de penalidade e ações de ressarcimento ao erário. Dependendo do tipo de irregularidades encontradas, podem-se tomar ações sumárias de descredenciamento do programa. Se a ação não ensejar essa atitude, então a equipe decide em uma multa a ser aplicada à farmácia pelas irregularidades encontradas até o valor de 10% do valor do dano causado. Toda farmácia que passa por esse processo é passível de sofrer multa dada a irregularidade.

Sobre a fase de ressarcimento, primeiro é necessário validar o RA recebido em termos de completude dos documentos, validação do nexo de causalidade entre os envolvidos e seus tempos de gestão administrativa (não técnica) da farmácia e a presença de todas as notificações do Denasus aos responsáveis auditados. Caso haja alguma falha documental detectada, é necessário retornar o RA ao Denasus para preenchimento das lacunas identificadas. Após essa validação primária do documento e a completude das informações comprovada, parte-se para a notificação (feita pelo DAF) ao CNPJ e CPFs envolvidos, no estrito limite do nexo temporal e causal de cada um. É importante lembrar que todos os sócios da farmácia, no limite das suas gestões até a data-fim da auditoria, respondem solidariamente por todo o processo. 

Antes de construir a notificação, é necessário consultar o SISGRU para verificar se não houve nenhum pagamento de ressarcimento anterior à notificação da parte de algum dos CPFs envolvidos ou em nome do CNPJ em auditoria. Caso haja pagamento total, o processo termina aqui. Caso haja pagamento parcial, os valores restantes serão atualizados e cobrados conforme o fluxo.

A notificação é construída a partir das seguintes informações: 

 1. informações pessoais da farmácia  (CNPJ) e dos CPFs envolvidos (obtida via SISTCE e comprovada via consultas no site da receita federal). Se a farmácia tiver um CNPJ ativo, esse será também notificado, em cima das notificações pessoais que cada um dos sócios recebe por CPF. Os CPFs são validados junto à Receita Federal para verificação da validade dos mesmos e também para saber se algum dos sócios veio a óbito no decorrer do período até a notificação (caso tenha sido, o processo corre contra os herdeiros);
 2. informações do Denasus sobre as glosas, ou seja, as transações realizadas com indícios materializados de fraude/irregularidade;
 3. informações sobre o domínio do débito, obtidas através de atualização monetária dos valores das glosas desde a data do fato até a data da notificação;
 4. GRU de pagamento dos valores (montante principal+atualização via SELIC, se for o caso), gerada via SISGRU. Essa GRU é gerada sempre com validade até o fim do mês de recebimento da notificação.

É necessária cautela na geração dessas informações, dado que o TCU é criterioso na garantia da ampla defesa e do contraditório aos responsáveis durante todo o processo. Caso o TCU não identifique a garantia desses direitos ou haja alguma incoerência/falta de informação, o processo retorna ao DAF com prazo marcado para reenvio ao TCU.

Todas as informações precisam estar relacionadas no SEI ao numero do processo envolvendo a auditoria. Todos os envios feitos devem ter seus Avisos de Recebimento e/ou devoluções escaneados e anexados ao processo. Caso alguma notificação não seja recebida pelo responsável, este deverá ser chamado via edital publicado no DOU. Apenas então pode-se considerar que a pessoa está ciente do processo aberto contra ela.

Caso a notificação não seja respondida/pagamento não tenha sido realizado, parte-se para a criação de uma nota técnica contendo todas as informações relativas ao processo, atualizadas até a data de emissão da Nota, também atualizando o valor do débito relativo ao processo.Assim sendo,o DAF solicita uma instauração de tomada de contas especial para o Fundo Nacional de Saúde que, por sua vez, como instaurador, solicita uma tomada de contas especial ao TCU, se o montante for maior do que R$ 100.000,00. Caso o montante seja menor do que este, o DAF encaminha por meio de despacho todas as informações necessárias para que o FNS realize o cadastramento de débito em aberto no Sistema e-TCE, para posterior encaminhamento à Procuradoria Geral da Fazenda Nacional, visando o acionamento judicial das partes.

#### Objetivo do sub-processo: penalizar as farmácias participantes do Programa Farmácia Popular do Brasil que cometeram atos contrários ao exercício do programa.

Após a chegada do RA, de autoria do Denasus, e separação entre as ações de restituição e penalidade, analisa-se a completude das informações do RA segundo à visão das possíveis penalidades (completude dos documentos necessários segundo a gravidade das ofensas). De acordo com uma primeira análise do RA, existe um conjunto de ofensas que permitem ao DAF prosseguir com o descredenciamento sumário de uma determinada farmácia (lembrando, sempre, que matrizes e filiais respondem como entes diferentes para o credenciamento: o descredenciamento de uma matriz não acarreta o descredenciamento das filiais, e vice-versa).

 - Hipóteses passíveis de descredenciamento sumário:
 - Descredenciamento por pedido judicial;
 - Descredenciamento a pedido do responsável legal;
 - Descredenciamento por irregularidades;
 - Descredenciamento por não-homologação dentro do período de testes
 - Descredenciamento por não renovação do RTA

Dentro do descredenciamento por irregularidades, é importante frisar o limite/barreira dos 16%. Esse limite implica que, se, durante uma determinada auditoria, mais de 16% do valor total dispensado entre (M-4) e (M-1) for irregular, a farmácia é passível de ser descredenciada sumariamente. No limiar dos 16%, é importante analisar o cenário completo da farmácia: reincidências, sócios em situação irregular com o DAF e situações semelhantes podem embasar um possível descredenciamento sumário. Em casos extremos, pode-se consultar a gestão do programa para tomada de decisão.

## Procedimentos Operacionais Padrão


#### Solicitar averiguação da empresa 

| **Processo:** | Sub-processo de Multa e Ressarcimento |
| --- | --- |
| **Atividade:** | Solicitar averiguação da empresa |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | DAF/SCTIE/MS |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| Indício materializado de fraude, seja advindo da equipe de monitoramento ou de outros órgãos externos ao DAF/SCTIE/MS. |

| **Procedimento da Atividade** |
| --- |
| Recebe-se o indício, verifica-se a factibilidade do mesmo (se vindo de agentes externos) e gera-se uma notificação de auditoria a ser realizada na empresa (ofício, cujo envio deve ser confirmado). Neste momento, deve ser aberto um número de processo junto ao SEI. Devem ser coletadas as assinaturas da CPFP e CGAFB. |

| **Saídas da Atividade** |
| --- |
| Solicitação de abertura de Processo de Auditoria para o Denasus, via ofício. |

| **Ações Corretivas** |
| --- |
|   |



#### Emitir ofício para a empresa

| **Processo:** | Sub-processo de Multa e Ressarcimento |
| --- | --- |
| **Atividade:** | Emitir ofício para a empresa |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | Denasus |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| Solicitação de abertura de Processo de Auditoria enviada pelo MS/SCTIE/DAF |

| **Procedimento da Atividade** |
| --- |
| Recebida a solicitação, deve ser expedido ofício para a farmácia (CNPJ) em questão, informando que a mesma está sob processo de auditoria, informando o período auditado e solicitando comprovação documental de todas as transações sob suspeita. Esse ofício deverá ser enviado via email e correios. Armazenar o Aviso de Recebimento do ofício dentro do processo devido no SEI. |

| **Saídas da Atividade** |
| --- |
| Ofício informando a farmácia em questão sobre o processo. Se a farmácia conseguir comprovar todas as transações em questão, ir para &quot;Recebe Comprovações&quot;. Caso contrário, ir para &quot;Elabora o RA&quot; |

| **Ações Corretivas** |
| --- |
|   |



#### Recebe Comprovações

| **Processo:** | Sub-processo de Multa e Ressarcimento |
| --- | --- |
| **Atividade:** | Recebe Comprovações |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | DAF/SCTIE/MS |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| Conjunto de documentos comprovando todas as transações sob suspeita, já analisados pelo Denasus. |

| **Procedimento da Atividade** |
| --- |
| Receber o conjunto de documentos enviados pela farmácia em questão indicando que a mesma está de acordo com os trâmites da portaria do Farmácia Popular. |

| **Saídas da Atividade** |
| --- |
| Fim do processo. Nenhuma outra ação necessária. |

| **Ações Corretivas** |
| --- |
|   |



#### Elaborar o RA

| **Processo:** | Sub-processo de Multa e Ressarcimento |
| --- | --- |
| **Atividade:** | Elaborar o RA |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | Denasus |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| Conjunto incompleto de documentos e/ou ausência de envio de documentos comprobatórios das transações sob suspeita. |

| **Procedimento da Atividade** |
| --- |
| Dado o conjunto de transações não comprovadas, deve-se elaborar o objeto chamado Relatório de Auditoria (RA): contendo todas as informações identificatórias tanto da farmácia quanto do conjunto dos sócios, assim como todas as informações relativas ao número, tipos, datas e valores das transações questionadas. Armazenar o RA dentro do SEI. |

| **Saídas da Atividade** |
| --- |
| Relatório de Auditoria (RA): relatório contendo todas essas informações consolidadas em um único objeto para análise e rastreabilidade das transações envolvidas, a ser enviado ao DAF para tratativas posteriores. |

| **Ações Corretivas** |
| --- |
|   |



#### Receber o RA

| **Processo:** | Sub-processo de Multa e Ressarcimento |
| --- | --- |
| **Atividade:** | Receber o RA |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | DAF/SCTIE/MS |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| RA: relatório elaborado pelo Denasus contendo todas as informações necessárias para o devido processo jurídico de acionamento das partes envolvidas. |

| **Procedimento da Atividade** |
| --- |
| O DAF recebe o RA, e neste momento o processo toma dois rumos paralelos: as ações de **multa** e de **ressarcimento**. As ações de multa são relativas às penalidades cabíveis à empresa por desvios em relação à portaria reguladora do Programa Farmácia Popular. As ações de ressarcimento são aquelas que tem o objetivo de ressarcir ao erário os valores pagos indevidamente à farmácia em questão. Importante notar que, caso o estabelecimento encaminhe documentos comprobatórios até aqui, o processo volta para &quot;Corrige/Completa o RA&quot;. |

| **Saídas da Atividade** |
| --- |
| Repasse do RA aos responsáveis pelas ações de multa e ressarcimento. |

| **Ações Corretivas** |
| --- |
|   |



#### Corrigir/Completar o RA

| **Processo:** | Sub-processo de Multa e Ressarcimento |
| --- | --- |
| **Atividade:** | Corrigir/Completar o RA |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | Denasus |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| RA indicado como incompleto, seja sob o ponto de vista das atitudes de multa/penalidade ou de ressarcimento. |

| **Procedimento da Atividade** |
| --- |
| Completar o RA de acordo com as lacunas apontadas pela equipe do DAF. |

| **Saídas da Atividade** |
| --- |
| RA com as lacunas informadas preenchidas de acordo com as solicitações da equipe do DAF. Armazenar o novo RA no SEI. |

| **Ações Corretivas** |
| --- |
|   |

#### Validar a Completude do RA

| **Processo:** | Sub-processo de Ressarcimento |
| --- | --- |
| **Atividade:** | Validar a Completude do RA |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | DAF/SCTIE/MS |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| RA: relatório elaborado pelo Denasus contendo todas as informações necessárias para o devido processo jurídico de acionamento das partes envolvidas. |

| **Procedimento da Atividade** |
| --- |
| Validar o RA de acordo com as informações necessárias ao ressarcimento: contrato social da farmácia, validade/existência do CNPJ, informações identificatórias dos sócios, situação do CPF de cada um, validação do nexo de causalidade entre os envolvidos e seus tempos de _gestão administrativa_ da farmácia e o envio de todas as notificações para todos os envolvidos na auditoria no estrito limite de suas responsabilidades temporais. É exigida cautela na geração dessas informações.  Importante notar que, caso o estabelecimento encaminhe documentos comprobatórios até aqui, o processo volta para &quot;Corrige/Completa o RA&quot;. |

| **Saídas da Atividade** |
| --- |
| Se o relatório estiver validado, prossegue-se para a coleta das informações dos sócios (&quot;Coleta dados CNPJ/CPFs dos sócios&quot;) e das glosas (eventos irregulares, &quot;Coleta informações das glosas&quot;). Se não, o relatório volta ao Denasus para que as lacunas identificadas sejam completadas (&quot;Corrige/Completa o RA&quot;) |

| **Ações Corretivas** |
| --- |
|   |



#### Coletar dados CNPJ/CPFs dos sócios

| **Processo:** | Sub-processo de Ressarcimento |
| --- | --- |
| **Atividade:** | Coletar dados CNPJ/CPFs dos sócios |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | DAF/SCTIE/MS |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| RA validado. |

| **Procedimento da Atividade** |
| --- |
| Coletar as informações pessoais da farmácia  (CNPJ) e dos CPFs envolvidos (obtida via SISTCE e comprovada via consultas no site da receita federal). Se a farmácia tiver um CNPJ ativo, esse será também notificado, em cima das notificações pessoais que cada um dos sócios recebe por CPF. Os CPFs são validados junto à Receita Federal para verificação da validade dos mesmos e também para saber se algum dos sócios veio a óbito no decorrer do período até a notificação (caso tenha sido, o processo corre contra os herdeiros). É importante lembrar que todos os sócios da farmácia, no limite das suas gestões até a data-fim da auditoria, respondem solidariamente por todo o processo. É exigida cautela na geração dessas informações. Armazenar essas informações no SEI. Em caso de recebimento de novo RA conclusivo da parte do Denasus, rever esse procedimento à luz do novo cenário. |

| **Saídas da Atividade** |
| --- |
| Informações identificatórias sobre o CNPJ e CPFs envolvidos, assim como situação cadastral. |

| **Ações Corretivas** |
| --- |
|   |



#### Coletar informações das glosas

| **Processo:** | Sub-processo de Ressarcimento |
| --- | --- |
| **Atividade:** | Coletar informações das glosas |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | DAF |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| RA validado. |

| **Procedimento da Atividade** |
| --- |
| Coletar as informações do Denasus sobre as glosas, ou seja, as transações realizadas com indícios materializados de fraude/irregularidade, através do site http://consultaauditoria.saude.gov.br/visao/pages/principal.html?2 , assim como suas datas e valores. É exigida cautela na geração dessas informações. Armazenar essas informações no SEI. Em caso de recebimento de novo RA conclusivo da parte do Denasus, rever esse procedimento à luz do novo cenário. |

| **Saídas da Atividade** |
| --- |
| Datas e valores relacionados ao período auditado. |

| **Ações Corretivas** |
| --- |
|   |



#### Verificar pagamento no SISGRU

| **Processo:** | sub-processo de ressarcimento |
| --- | --- |
| **Atividade:** | Verificar pagamento no SISGRU |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | DAF/SCTIE/MS |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| Informações identificatórias da farmácia (CNPJ) e dos CPFs dos sócios. |

| **Procedimento da Atividade** |
| --- |
| Verificar se houve algum pagamento (parcial ou total) dos valores envolvidos na auditoria, seja da parte do CNPJ ou de algum dos CPFs dos sócios envolvidos. É necessário consultar o SISGRU para verificar se não houve nenhum pagamento de ressarcimento anterior à notificação da parte de algum dos CPFs envolvidos ou em nome do CNPJ em auditoria. É exigida cautela na geração dessas informações. Armazenar essas informações no SEI. |

| **Saídas da Atividade** |
| --- |
| Caso haja pagamento total, o processo termina aqui, sem mais ações a serem tomadas. Caso haja pagamento parcial, os valores restantes serão atualizados e cobrados conforme o fluxo (&quot;Atualiza os valores envolvidos&quot;). |

| **Ações Corretivas** |
| --- |
|   |



#### Atualizar os valores envolvidos

| **Processo:** | Sub-processo de Ressarcimento |
| --- | --- |
| **Atividade:** | Atualizar os valores envolvidos |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | DAF/SCTIE/MS |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| Informações dos valores não-restituídos ao erário da parte da farmácia ou dos sócios. |

| **Procedimento da Atividade** |
| --- |
| Atualiza-se o domínio do débito, obtido através de atualização monetária dos valores das glosas desde a data do fato até a data da notificação (planejada para o dia 20-25 do mês corrente). Em caso de recebimento de novo RA conclusivo da parte do Denasus, rever esse procedimento à luz do novo cenário. |

| **Saídas da Atividade** |
| --- |
| Valores atualizados do quanto o conglomerado (farmácia e sócios) devem restituir ao erário. |

| **Ações Corretivas** |
| --- |
|   |



#### Gerar GRU para pagamento

| **Processo:** | Sub-processo de Ressarcimento |
| --- | --- |
| **Atividade:** | Gerar GRU para pagamento |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | DAF/SCTIE/MS |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| Valores atualizados do quanto o conglomerado (farmácia e sócios) devem restituir ao erário. |

| **Procedimento da Atividade** |
| --- |
| Gerar a GRU de pagamento dos valores (montante principal+atualização via SELIC, se for o caso), gerada via SISGRU. Essa GRU é gerada sempre com validade até o fim do mês de envio da notificação. Armazenar essa GRU no SEI. |

| **Saídas da Atividade** |
| --- |
| Guia de Recebimento da União contendo os valores que devem ser restituídos ao erário da parte da farmácia e dos sócios. |

| **Ações Corretivas** |
| --- |
|   |



#### Notificar a empresa e os sócios

| **Processo:** | Sub-processo de Ressarcimento |
| --- | --- |
| **Atividade:** | Notificar a empresa e os sócios |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | DAF/SCTIE/MS |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| Informações identificatórias do CNPJ e dos sócios; informações sobre as glosas; informações sobre o domínio do débito e a GRU para restituição dos valores ao erário. |

| **Procedimento da Atividade** |
| --- |
| Gerar notificação ao CNPJ e aos CPFs dos sócios, a ser enviada para os endereços já coletados. Armazenar o Aviso de Recebimento no SEI. Caso alguma notificação não seja recebida pelo responsável, este deverá ser chamado via edital publicado no DOU. Apenas então pode-se considerar que a pessoa está ciente do processo aberto contra ela. Armazenar notificação e avisos de recebimento, ou publicação no DOU, no SEI. |

| **Saídas da Atividade** |
| --- |
| O prazo para resposta é de 30 dias a partir do recebimento da notificação. Caso o estabelecimento tenha respondido com documentos comprobatórios, retornar ao Denasus com a nova informação para nova análise à luz dos documentos recebidos. Caso o estabelecimento tenha pago a GRU por completo, o processo termina aqui, sem mais ações. Caso o estabelecimento não tenha dado resposta alguma, parte-se para a Nota Técnica (&quot;Elabora Nota Técnica&quot;) |

| **Ações Corretivas** |
| --- |
|   |



#### Elaborar Nota Técnica

| **Processo:** | Sub-processo de Ressarcimento |
| --- | --- |
| **Atividade:** | Elaborar Nota Técnica |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | DAF/SCTIE/MS |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| Ausência de resposta ou resposta incompleta do estabelecimento posterior à notificação. |

| **Procedimento da Atividade** |
| --- |
| Cria-se uma nota técnica contendo todas as informações relativas ao processo, atualizadas até a data de emissão da Nota, também atualizando o valor do débito relativo ao processo. Armazenar Nota Técnica no SEI. Em caso de recebimento de novo RA conclusivo da parte do Denasus, rever esse procedimento à luz do novo cenário. |

| **Saídas da Atividade** |
| --- |
| Nota Técnica contendo e sumarizando todas as informações do processo até a data de emissão da nota. Se o valor da restituição for igual ou maior a R$ 100.000,00, encaminhar ao FNS para Tomada de Contas Especial (&quot;Encaminha insumos para TCE ao FNS&quot;). Caso contrário, encaminhar ao FNS para prosseguimento do processo junto à PGFN (&quot;Encaminha insumos para a PGFN ao FNS&quot;) |

| **Ações Corretivas** |
| --- |
|   |



#### Encaminhar insumos para TCE ao FNS

| **Processo:** | Sub-processo de Ressarcimento |
| --- | --- |
| **Atividade:** | Encaminhar insumos para TCE ao FNS |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | DAF/SCTIE/MS |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| Nota Técnica contendo todas as informações do processo até a data, cujo valor do ressarcimento é maior ou igual a R$ 100.000,00 |

| **Procedimento da Atividade** |
| --- |
| O DAF solicita uma instauração de tomada de contas especial para o Fundo Nacional de Saúde que, por sua vez, como instaurador, solicita uma tomada de contas especial ao TCU, se o montante for maior do que R$ 100.000,00. |

| **Saídas da Atividade** |
| --- |
| Fim do processo. O DAF encaminha a solicitação. |

| **Ações Corretivas** |
| --- |
|   |



#### Encaminhar insumos para a PGFN ao FNS

| **Processo:** | Sub-processo de Ressarcimento |
| --- | --- |
| **Atividade:** | Encaminhar insumos para a PGFN ao FNS |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | DAF/SCTIE/MS |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| Nota Técnica contendo todas as informações do processo até a data, cujo valor do ressarcimento é menor do que R$ 100.000,00 |

| **Procedimento da Atividade** |
| --- |
| O DAF encaminha por meio de despacho todas as informações necessárias para que o FNS realize o cadastramento de débito em aberto no Sistema e-TCE, para posterior encaminhamento à Procuradoria Geral da Fazenda Nacional, visando o acionamento judicial das partes. |

| **Saídas da Atividade** |
| --- |
| Fim do processo. O DAF encaminha o despacho. |

| **Ações Corretivas** |
| --- |
|   |



#### Validar a completude do RA

| **Processo:** | Sub-processo de Multa |
| --- | --- |
| **Atividade:** | Validar a completude do RA |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | DAF/SCTIE/MS |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| RA: relatório elaborado pelo Denasus contendo todas as informações necessárias para o devido processo jurídico de acionamento das partes envolvidas. |

| **Procedimento da Atividade** |
| --- |
| Analisar a completude das informações do RA segundo à visão das possíveis penalidades (completude dos documentos necessários segundo a gravidade das ofensas). De acordo com uma primeira análise do RA, existe um conjunto de ofensas que permitem ao DAF prosseguir com o descredenciamento sumário de uma determinada farmácia (lembrando, sempre, que matrizes e filiais respondem como entes diferentes para o credenciamento: o descredenciamento de uma matriz não acarreta o descredenciamento das filiais, e vice-versa). Importante notar que, caso o estabelecimento encaminhe documentos comprobatórios até aqui, o processo volta para &quot;Corrige/Completa o RA&quot;. |

| **Saídas da Atividade** |
| --- |
| Se o RA estiver completo de acordo com a visão da penalidade/multa, o processo segue para a análise das irregularidades (Analisa Irregularidades). Se não, o relatório volta ao Denasus para que as lacunas identificadas sejam completadas (&quot;Corrige/Completa o RA&quot;) |

| **Ações Corretivas** |
| --- |
|   |



#### Analisar Irregularidades

| **Processo:** | Sub-processo de Multa |
| --- | --- |
| **Atividade:** | Analisar Irregularidades |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | DAF/SCTIE/MS |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| RA validado. |

| **Procedimento da Atividade** |
| --- |
| Analisar as irregularidades envolvidas no RA e verificar se a farmácia cometeu alguma infração grave (motivando descredenciamento) ou se ela cometeu apenas infração passível de multa. Caso ela tenha incorrido em irregularidades graves:
- pedido judicial;
- infrações graves tais como uso de CPF de falecidos;
- ou alto percentual de irregularidades (soma dos valores das competências de todo o período auditado dividido pelo valor a ser ressarcido ao FNS maior que 16%)
o estabelecimento será descredenciado. No limiar dos 16% (15,99%), é importante analisar o cenário completo da farmácia: reincidências, sócios em situação irregular com o DAF e situações semelhantes podem embasar um possível descredenciamento sumário. Em casos extremos, pode-se consultar a gestão do programa para tomada de decisão. Em caso de recebimento de novo RA conclusivo da parte do Denasus, rever esse procedimento à luz do novo cenário. |

| **Saídas da Atividade** |
| --- |
| Decisão sobre a penalidade adequada à farmácia (multa e descredenciamento ou só multa). |

| **Ações Corretivas** |
| --- |
|   |


#### Calcular multa devida

| **Processo:** | Sub-processo de Multa |
| --- | --- |
| **Atividade:** | Calcular multa devida |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | DAF/SCTIE/MS |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| RA validado e análise das irregularidades encontradas. |

| **Procedimento da Atividade** |
| --- |
| Calcular o valor da multa. Esse valor é o maior entre ambos estes valores: ou o total das transações fora da portaria ou 10% (dez por cento) do montante das vendas efetuadas no âmbito do PFPB (referente aos últimos 3 (três) meses completos consolidados das autorizações). Em caso de recebimento de novo RA conclusivo da parte do Denasus, rever esse procedimento à luz do novo cenário. |

| **Saídas da Atividade** |
| --- |
| Valor da multa a ser aplicada ao estabelecimento. Caso a empresa seja apenas multada, gerar GRU para pagamento (&quot;Gera GRU para pagamento&quot;). Caso ela seja também descredenciada, partir para o descredenciamento da mesma (&quot;Descredencia a empresa&quot;). |

| **Ações Corretivas** |
| --- |
|   |



#### Gerar GRU para pagamento

| **Processo:** | Sub-processo de Multa |
| --- | --- |
| **Atividade:** | Gerar GRU para pagamento |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | DAF/SCTIE/MS |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| Valor da multa a ser aplicada ao estabelecimento. |

| **Procedimento da Atividade** |
| --- |
| Gera a GRU para pagamento do valor da multa devida. |

| **Saídas da Atividade** |
| --- |
| GRU de pagamento da multa. |

| **Ações Corretivas** |
| --- |
|   |



#### Descredenciar a empresa

| **Processo:** | Sub-processo de Multa |
| --- | --- |
| **Atividade:** | Descredenciar a empresa |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | DAF/SCTIE/MS |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| Decisão de descredenciar a empresa baseado nas hipóteses previstas pela portaria. |

| **Procedimento da Atividade** |
| --- |
| Descredencia a empresa junto ao programa Farmácia Popular, lembrando, sempre, que matrizes e filiais respondem como entes diferentes para o credenciamento: o descredenciamento de uma matriz não acarreta o descredenciamento das filiais, e vice-versa. Em caso de recebimento de novo RA conclusivo da parte do Denasus, analisar se a decisão de descredenciamento continua (&quot;Analisar irregularidades&quot;). |

| **Saídas da Atividade** |
| --- |
| Bloqueio no cadastro de farmácias do programa. |

| **Ações Corretivas** |
| --- |
|   |



#### Emitir Nota Técnica

| **Processo:** | Sub-processo de Multa |
| --- | --- |
| **Atividade:** | Emitir Nota Técnica |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | DAF/SCTIE/MS |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| Decisão de bloquear a empresa baseado nas hipóteses previstas pela portaria. |

| **Procedimento da Atividade** |
| --- |
| Gera Nota Técnica sumarizando todas o processo segundo a decisão de descredenciar a empresa do programa.  Em caso de recebimento de novo RA conclusivo da parte do Denasus, analisar se a decisão de descredenciamento continua (&quot;Analisar irregularidades&quot;) |

| **Saídas da Atividade** |
| --- |
| Nota Técnica contendo e sumarizando todas as informações do processo até a data de emissão da nota. |

| **Ações Corretivas** |
| --- |
|   |



#### Publicar no DOU

| **Processo:** | Sub-processo de Multa |
| --- | --- |
| **Atividade:** | Publicar no DOU |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | DAF/SCTIE/MS |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| Nota Técnica contendo e sumarizando todas as informações do processo até a data de emissão da nota. |

| **Procedimento da Atividade** |
| --- |
| Encaminha o descredenciamento, após assinado pelo secretário, para publicação no DOU. |

| **Saídas da Atividade** |
| --- |
| Publicação no DOU comunicando o descredenciamento da farmácia junto ao programa. |

| **Ações Corretivas** |
| --- |
|   |



#### Gerar ofício de pagamento

| **Processo:** | Sub-processo de Multa |
| --- | --- |
| **Atividade:** | Publicar no DOU |
| **Executor (nome):** |   |
| **Elaborado em:** | 31/07/2019 |
| **Revisado em:** | 06/08/2019  |
| **Unidade Responsável:** | DAF/SCTIE/MS |
| **Duração (estimada):** |   |

| **Entradas da Atividade** |
| --- |
| Publicação no DOU indicando o descredenciamento da farmácia e valor da multa a ser aplicada ao estabelecimento. |

| **Procedimento da Atividade** |
| --- |
| Gera ofício de pagamento relacionado à multa aplicada à farmácia. |

| **Saídas da Atividade** |
| --- |
| Fim do processo. Sem mais ações a serem tomadas. |

| **Ações Corretivas** |
| --- |
|   |

## Micro serviços

Para o processo de multa e ressarcimento foi desenvolvido um estudo para utilização de micro serviço integrador ao SISGRU para simplificar o processo de geração de Guias de recolhimento e para centralizar o trabalho em uma única ferramenta.

## Especificação
