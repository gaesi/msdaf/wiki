---
title: Estudo da Portaria nº 111, de 28 de janeiro de 2016 do MS
date: 2019-04-01 16:33:34
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

categories:
# - [Orçamento, Painel Orçamentário]
# - [Orçamento, PLOA automatizada]
# - [Preditivo, Levantamento Epidemiológico]
# - [Preditivo, Previsão de compras]
- [Farmácia Popular, Monitoramento]
- [Farmácia Popular, Notificação]
- [Farmácia Popular, Ressarcimento]
# - [Capacitação, Cursos]
# - [Capacitação, Eventos]
# - [Capacitação, Workshops]
---


Analise realizada sob a Portaria em destaque para obtenção de características de regras que possam ser usadas para desenvolvimento de aplicações para detecção de fraudes bem como a identificação de processos mal aplicados.

Primeiramente apresenta-se o Programa e suas características para que possamos estudar a Portaria.

## Apresentação do Programa Farmácia Popular
### O que é o Programa Farmácia popular
O Programa Farmácia Popular do Brasil foi criado com o objetivo de oferecer mais uma alternativa de acesso da população aos medicamentos considerados essenciais. O Programa cumpre uma das principais diretrizes da Política Nacional de Assistência Farmacêutica. [Fonte: Site Ministério da Saúde]

### Funcionamento
Atualmente, o Programa “Aqui tem Farmácia Popular" funciona por meio do credenciamento de farmácias e drogarias comerciais, aproveitando a dinâmica da cadeia farmacêutica (produção x distribuição x varejo). São oferecidos medicamentos gratuitos para hipertensão (pressão alta), diabetes e asma, além de medicamentos com até 90% de desconto indicados para dislipidemia (colesterol alto), rinite, Parkinson, osteoporose e glaucoma. Ainda pelo sistema de copagamento, o Programa oferece anticoncepcionais e fraldas geriátricas.[ Fonte: Site Ministério da Saúde]

## Analise da Portaria
- Na *Subseção II* trata da dispensação do medicamento onde deve ser gerado um documento chamado **Cupom Vinculado** juntamente com o documento fiscal. Nele deve ter os seguintes dados

	- nome completo do beneficiário
	- CPF do beneficiário
	- Assinatura do beneficiário
	- Endereço do beneficiário
	- Razão social e CNPJ da empresa;
	- Nome do responsável legal da empresa
	- Número de autorização do DATASUS
	- CRM do médico
	- Valor total da venda
	- Data da compra
	- Nome e apresentação do medicamento
	- Código de barras do medicamento
	- posologia diária ou prescrição diária
	- quantidade autorizada
	- saldo atual
	- data da próxima compra
	- identificação do operador da transação
	- número da Ouvidoria do Ministério da Saúde

- No *Art 35* fala sobre o ADM (Autorizações de Dispensação de Medicamentos e Correlatos). É um documento eletrônico e o MS pode verificá-lo mensalmente ou quando houver necessidade.

- Regras que se descumpridas recebem penalidades
	- Comercializar ou dispensar medicamentos fora da lista
	- Não exigir documentos solicitados na ADM
	- Não cobrar do paciente a parcela referente a compra do medicamento
	- Comercializar medicamentos em nome de terceiros
	- Estornar venda irregular ou cancelada depois de 7 dias
	- Utilizar senha que não pertence ao estabelecimento
	- Firmar convênio e parceria com terceiros no âmbito do PFPB
	- Fazer uso publicitado do programa fora da regra estabelecida
	- Cadastrar paciente em nome do Programa
	- Entregar o medicamento fora do estabelecimento
	- Desrespeitar regras da vigilância sanitária
	- Permitir q terceiros assinem pelo beneficiário (fora em casos permitidos)
	- Rasurar documentos necessários para validação
	- Receber a prescrição, laudo ou atestado depois da autorização
	- Lançar no sistema informações divergentes das constantes na prescrição e no documento do paciente
	- Duplicidade de dispensação para o mesmo paciente
	- Substituir medicamentos prescritos de acordo com a legislação
	- Dispensar medicamento com código de barra diferente do cadastrado no sistema

- Na *Subseção VI* trata do documento ADM:

- O processo de aceitação tem 3 fases e a cada uma são solicitados dados à empresa. Envio dos dados Principais, Dados Fiscais e Listagem dos medicamentos com os valores

- Primeira Fase:
	- código da solicitação
	- CNPJ do estabelecimento
	- CPF do paciente
	- CRM/RMS do médico que emitiu a prescrição
	- UF do CRM do médico
	- data de emissão da prescrição
	- identificador da transação
	- lista de medicamentos e correlatos:
		- código de barras da apresentação do medicamento
		- quantidade solicitada
		- valor unitário
		- quantidade diária prescrita
	- “login” da farmácia e drogaria
	- senha da farmácia e drogaria
	- “login” do atendente da farmácia e drogaria
	- senha do atendente da farmácia e drogaria

- Após receber a confirmação da primeira fase informará os dados da segunda fase:
	- código da solicitação enviado na primeira fase
	- número da pré-autorização recebido
	- número do documento fiscal gerado pelo estabelecimento
	- “login” da farmácia e drogaria
	- senha da farmácia e drogaria
	- “login” do atendente da farmácia e drogaria
	- senha do atendente da farmácia e drogaria

- Após receber a confirmação da segunda fase informará os dados da terceira fase:
	- número da pré-autorização
	- número do documento fiscal gerado pelo estabelecimento
	- lista de medicamentos e correlatos autorizados com as seguintes informações:
		- código de barras da apresentação do medicamento e do correlato
		- quantidade autorizada em UP
		- valor da parcela do Ministério da Saúde informado pelo Sistema Autorizador
		- valor da parcela do paciente informada pelo Sistema Autorizador
	- “login” da farmácia e drogaria;
	- senha da farmácia e drogaria;
	- “login” do atendente da farmácia e drogaria
	- senha do atendente da farmácia e drogaria.

- Após isso ele receberá a confirmação e autorização.
￼
## Referências
[Site Ministério da Saúde](http://portalms.saude.gov.br/acoes-e-programas/farmacia-popular/sobre-o-programa)
[Portaria nº111 de 28/01/2016](http://bvsms.saude.gov.br/bvs/saudelegis/gm/2016/prt0111_28_01_2016.html)
