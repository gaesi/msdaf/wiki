---
title: Lista de Relatórios
date: 2019-07-03 12:23:04
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

category:
- [Preditivo, Relatórios]

tags:
---

* Relatórios de Inteligência Artificial e Ciência de Dados
    * <a href="/msdaf/wiki/pdfs/(pt)DataReport.pdf" target="_blank">Relatório de Qualidade dos Dados</a>
    * <a href="/msdaf/wiki/pdfs/CNES BD.pdf" target="_blank">Relatório de dados do Sistema CNES</a>
    * <a href="/msdaf/wiki/pdfs/exploratory_analisys.pdf" target="_blank">Relatório de análise exploratória (Farmácia Popular)</a>
    * <a href="/msdaf/wiki/pdfs/fonte_dados_mag.pdf" target="_blank">Relatório de fontes de dados MAG</a>
    * <a href="/msdaf/wiki/pdfs/MLPMicroset.pdf" target="_blank">Relatório MLP com Micro Dataset</a>
* Processos
    * <a href="/msdaf/wiki/pdfs/01_01 Credenciamento.pdf" target="_blank">Projeto de Credenciamento</a>
    * Macroprocesso 01.01 Solicitar Credenciamento no PFPB
        * <a href="/msdaf/wiki/pdfs/01_01_01 Validar Dados da Farmácia.pdf" target="_blank">Validar Dados do Cadastro da Farmácia</a>
        * <a href="/msdaf/wiki/pdfs/01_01_02 Teste em Homologação.pdf" target="_blank">Realizar teste em homologação</a>
        * <a href="/msdaf/wiki/pdfs/02_02 Ressarcimento.pdf" target="_blank">Preparar ressarcimento mensal do PFPB</a>
    * BPMN
        * <a href="/msdaf/wiki/pdfs/Coord - Elaborar Termo de Referência v2.pdf" target="_blank">Elaborar Termo de Referência</a>
        * <a href="/msdaf/wiki/pdfs/Coord - Monitorar Estoque_Calamidade Pública.pdf" target="_blank">Monitorar Estoque Calamidade Pública</a>
        * <a href="/msdaf/wiki/pdfs/Coord - Monitorar Estoque_Saúde da Mulher e Insulina.pdf" target="_blank">Monitorar Estoque_Saúde da Mulher e Insulina</a>
        * <a href="/msdaf/wiki/pdfs/Coord - Planejar Programação_Saúde da Mulher e Insulina.pdf" target="_blank">Planejar Programação_Saúde da Mulher e Insulina</a>
        * <a href="/msdaf/wiki/pdfs/Coord - Realizar Aquisição Centralizada por Pregão e IRP.pdf" target="_blank">Realizar Aquisição Centralizada por Pregão e IRP</a>
        * <a href="/msdaf/wiki/pdfs/Coord - Realizar Aquisição Centralizada v2.pdf" target="_blank">Realizar Aquisição Centralizada</a>        
* Educacional e Seminários
    * <a href="/msdaf/wiki/pdfs/AssistFarm40.pdf" target="_blank">Assistência Farmacêutica 4.0: caminhos para a implementação</a>
        
