---
title: Modelo de dados para Solicitação de Esclarecimento
date: 2019-05-10 15:14:20
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

tags:
categories:
# - [Orçamento, Painel Orçamentário]
# - [Orçamento, PLOA automatizada]
# - [Preditivo, Levantamento Epidemiológico]
# - [Preditivo, Previsão de compras]
- [Farmácia Popular, Monitoramento]
# - [Farmácia Popular, Notificação]
# - [Farmácia Popular, Ressarcimento]
# - [Capacitação, Cursos]
# - [Capacitação, Eventos]
# - [Capacitação, Workshops]
---
# Modelo de Entidades e Relacionamentos Para Solicitação de Esclarecimento

O processo de solicitação de esclarecimento envolve hoje um trabalho manual de fornecimento e inserção de dados relativos a processos internos do Ministério. Os dados relativos aos protocolos de autorização de ressarcimento e os tipos de ocorrência de irregularidades são utilizados para gerar uma solicitação de esclarecimento de tais irregularidades por parte da Farmácia cadastrada.

O modelo de dados abaixo ilustra a relação entre o esclarecimento, as autorizações vinculadas a ele, de acordo com um tipo de autorização previamente fornecido e a lista de tipos de ocorrência nas quais a farmácia apresentou algum tipo de irregularidade.

![MER das solicitações de esclarecimento](https://gaesi.gitlab.io/msdaf/wiki/images/post-mer-esclarecimento/mer-esclarecimento.png "MER das solicitações de esclarecimento")

Cada autorização adicionada a uma solicitação de esclarecimento está relacionada a um tipo de autorização, previamente cadastrada, que possui um código, data e medicamento vinculados. A ela são incorporadas ocorrências que são utilizadas para gerar o corpo do parágrafo de solicitação de esclarecimento.


