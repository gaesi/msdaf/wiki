---
title: GreenPlum para ganho de escala no uso de BigData.
date: 2019-07-23 10:05:04
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

category:
# - [Orçamento, Painel Orçamentário]
# - [Orçamento, PLOA automatizada]
# - [Preditivo, Levantamento Epidemiológico]
# - [Preditivo, Previsão de compras]
 - [Preditivo, Tecnologias Aplicadas]
# - [Farmácia Popular, Monitoramento]
# - [Farmácia Popular, Notificação]
# - [Farmácia Popular, Ressarcimento]
# - [Capacitação, Cursos]
# - [Capacitação, Eventos]
# - [Capacitação, Workshops]

tags:
---



## Motivação

O 

### Modelo de arquitetura proposta
A iniciativa de gerar o estudo partiu do Datasus e o estudo permite entender de forma mais objetivo os ganhos e perdas da implantação da solução greenplum-Postgress. A equipe BNAFAR compartilho o desenho da arquitetura sugerida pelo DATASUS.

![Modelo de gestão de dados](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/greenplum/propostaGreem.jpeg)

O esquemático sugere a utilização do Greenplum como ferramenta para o processamento analítico e repositório/lago de dados. Esta arquitetura deverá permitir a gestão de dados protegidos e o desenvolvimento um modelo de distribuição de dados escalável.

## Greenplum

### O que é

O Greenplum é um banco de dados desenvolvido sob o [Postgresql](https://www.postgresql.org/) para gestão de grandes volumes de dados permitindo consultas e inserções massivas e paralelas com foco em analitcs, inteligência e aprendizado de máquina. O esquemático ilustra de forma genérica como o Greenplum atuaria intermediando o processamento, coleta e provimento de dados para diferentes objetivos.

![NextGenDataPlatform](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/greenplum/NextGenDataPlatform.png)

Dentre as principais características que tornam o Greenplum uma boa opção para gestão e unificação de dados são:
 - Gestão de permissão e acesso facilitado aos dados para múltiplos usuários
 - Facilidade de integração com ferramentas de análise e processamento de dados
 - Simplicidade para integração com ferramentas por meio de plugins e interfaces nativas

### Instalação

Foi desenvolvido a critério de teste, uma [imagem Docker do GreenPLum](https://hub.docker.com/r/gaesi/greenplum) para possibilitar a configuração e inicialização do banco. Para instalar basta executar o comando:
```
docker run -it --hostname=gpdbsne --name greenplum --publish 5432:5432 --publish 88:22 gaesi/greenplum bin/bash
```
Por padrão, a imagem não inicializa o banco. Para tal, é necessário executar o seguinte comando dentro do container:
```
startGPDB.sh
```
Após isso, acesse o banco com o usuário `gpadmin` e gerencie os bancos de dados de forma semelhante ao Postgres com o `psql`
```
su - gpadmin
psql
```
Para editar configurações de segurança e restrições de acesso, basta editar o arquivo `pg_hba.conf` que se encontra no diretório `/gpdata/master/gpseg-1`. Após qualquer alteração, é necessário reinicializar o banco, com os seguintes comandos:
```
su - gpadmin
gpstop
gpstart
```
Para mais informações sobre configurações adicionais acesse a [documentação oficial](https://gpdb.docs.pivotal.io/5210/main/index.html).

### Capacidades

 - Utilização de ferramentas que utilizam postgres como banco de dados como é o caso do dataverse
 - Possibilidade de execução de treinamento de algoritmos de aprendizado de máquina diretamente em banco de dados, tornando desnecessário o uso de ferramentas de ETL e de duplicação de dados. 
 - Possibilita a importação em paralela de dados por meio do uso de segmentos independentes.

## Conclusão

Dentro do escopo do projeto o GreenPlum é adequado a necessidade do DAF, assim como da suporte aos algorítimos e metodologias de ML e IA que estão em continuo desenvolvimento.

De acordo com a pesquisa desenvolvida, como o foco na criação de um lago de dados, a arquitetura do greenplum permitira o desenvolvimento de forma escalável da proposta que o DAF procura desenvolver

O Greenplum permite reduzir os pontos de armazenamentos de dados fornecendo um ambiente único e escalável para convergir cargas de trabalho analíticas e operacionais, como o fluxo de processamento. Execução consultas pontuais, ingestão rápida de dados, exploração de dados científicos e consultas de relatórios de longa duração com maior escala e simultaneidade.

Além de alavancar um grande conjunto de recursos do Postgres, a Greenplum oferece aprendizado de máquina, análise gráfica, estatística, transformação de dados, analítica de texto, geoespacial, múltiplas bibliotecas de dados e conectividade a ferramentas de produtividade como Jupyter e Apache Zeppelin. Essas inovações são todas compatíveis com o PostgreSQL, mas projetadas para casos de uso massivamente paralelos.
