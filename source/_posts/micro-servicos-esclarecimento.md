---
title: Automações no processo de Análise de irregularidades - Farmácia Popular
date: 2019-07-13 17:17:54
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

tags:
categories:
# - [Orçamento, Painel Orçamentário]
# - [Orçamento, PLOA automatizada]
# - [Preditivo, Levantamento Epidemiológico]
# - [Preditivo, Previsão de compras]
- [Farmácia Popular, Monitoramento]
# - [Farmácia Popular, Notificação]
# - [Farmácia Popular, Ressarcimento]
# - [Capacitação, Cursos]
# - [Capacitação, Eventos]
# - [Capacitação, Workshops]
---

O procedimento de análise e identificação de irregularidades nas autoraizações registradas pelas farmácias cadastradas no programa Farmácia Popular é em grande parte manual. O trabalho de avaliação documental comparando os documentos recebidos com as especificações do programa possui pontos passiveis de automação identificados e explorados pela automação do processo de solicitação de esclarecimento.

## Manutenção de tipos de ocorrência

Os tipos de ocorrência para requisição de esclarecimentos às farmácias são padronizados para agilizar o processo de construções dos ofícios. Entretanto, com a falta de sistematização desses padrões, comumente os analistas se encontram com padrões divergentes e precisam realinhar os modelos para nova padronização. Com a automação do processo de registro dos tipos de ocorrência, é possível centralizar o padrão textual e numérico de cada tipo de ocorrência e com isso simplificar a confecção dos ofícios.

## Registro de ocorrências por autorização
Cada autorização efetuada no sistema autorizador do Farmácia Popular possui um identificador único vinculado ao CNPJ da farmácia, o analista que avalia as autorizações precisa avaliar se a autorização está coerente ou se são encontradas ocorrências de irregularidades para a autorização e para cada medicamento vinculado. Esse processo é feito de forma manual, com potencial de falha humana que pode comprometer todo o processo de multa ou ressarcimento caso incorra em problemas nas solicitações efetuadas à farmácia.

Com a automação do processo de registro de ocorrências, o procedimento é simplificado de forma que o registro e controle das ocorrências por autorização são sistematizados, com facilidades de monitoramento e execução de ajustes, auxiliando no controle das ocorrência e na confecção da devolutiva de solicitações por CNPJ (réplica quanto aos documentos recebidos). Com o apresentado, conclui-se que a utilização desta ferramenta contribui no ganho de produtividade do serviço executado pelos profissionais do setor, ganho adquirido pela redução do trabalho manual e reduzindo re-trabalho em caso de erro, corroborando assim pela utilização da mesma.

## Registro de solicitações de esclarecimento

Além do registro das ocorrências por autorização, foi automatizado também o processo de manutenção dos registros de solicitação de esclarecimento por CNPJ. Nesse sentido, as ocorrências de cada farmácia e as autorizações pertinentes a elas são cadastradas no sistema, simplificando o processo de identificação das autorizações e suas devidas ocorrências, a identificação e execução de ajustes na solicitação gerada, como adição de novas autorizações, ou novas ocorrências e ainda a possibilidade de controle do trabalho executado por cada analista.

## Geração de lista de itens solicidados

Com o registro de autorizações, ocorrências e solicitações de esclarecimento, foi possível desenvolver também a geração automática da lista de itens solicitados. Esse é o procedimento mais importante na construção do ofício que demanda o envia de justificativas e/ou documentações por parte das farmácias para cada uma das ocorrências identificadas para as autorizações com irregularidades. Cada ocorrência possui uma descrição textual que pode conter o nome do medicamento vinculado, a data e o código da autorização. Atualmente, o procedimento é feito de forma manual e podem acontecer erros de português, ou nos números de identificação que podem acarretar em mais delongas burocráticas. Com a automação da geração dos parágrafos, o tempo e custo de alocação de recursos humanos, bem como a incidência de falhas foram reduzidas.

Com essa funcionalidade, objetiva obter ganho significativo de produtividade nos trabalhos executados, não sendo necessário o acesso a programas de edição de texto para realizar as atividades, limitando-se apenas a um ambiente.


A utilização das ferramentas desenvolvidas, análise e comparação dos procedimento apresentados acima resultou em um ganho de tempo considerável dando a possibilidade da realização de mais tarefas no mesmo espaço de tempo, respeitando assim o Princípio da Eficiência incluso das diretrizes do serviço público pela reforma administrativa realizada em 1998 e como citado por *Alexandre de Morais*, hoje Ministro no STF, em seu livro **Direito Constitucional** de 1999, que diz:

> *“aquele que impõe à Administração Pública direta e indireta a seus agentes a persuasão do bem comum, por meio do exercício de suas competências de forma imparcial, neutra, transparente, participativa, eficaz, sem burocracia e sempre em busca da qualidade, primando pela adoção dos critérios legais e morais necessários para a melhor utilização possível dos recursos públicos, de maneira a evitar-se desperdícios e garantir-se maior rentabilidade social. Note-se que não se trata da consagração da tecnocracia, muito pelo contrário, o princípio da eficiência dirige-se para a razão e fim maior do Estado, a prestação dos serviços essenciais à população, visando à adoção de todos os meios legais e morais possíveis para a satisfação do bem comum”*  

**MORAES**, *Alexandre de.* **Direito Constitucional** *. 7. Ed. 	São Paulo: Atlas, 1999, p. 294.*


Com isso conclui-se que a aplicação dos micros-serviços desenvolvidos é essencial para o bom aproveitamento do tempo empregado na realização das atividades dos profissionais do setor Farmácia Popular.
