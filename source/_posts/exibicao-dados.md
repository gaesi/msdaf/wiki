---
title: Técnicas para melhor Visualização de Dados.
date: 2019-07-23 10:05:04
toc: true
sidebar:
    left:
        sticky: true
widgets:
-
    type: toc
    position: left
categories:
# - [Orçamento, Painel Orçamentário]
# - [Orçamento, PLOA automatizada]
# - [Preditivo, Levantamento Epidemiológico]
# - [Preditivo, Previsão de compras]
- [Preditivo, Tecnologias Aplicadas]
# - [Farmácia Popular, Monitoramento]
# - [Farmácia Popular, Notificação]
# - [Farmácia Popular, Ressarcimento]
# - [Capacitação, Cursos]
# - [Capacitação, Eventos]
# - [Capacitação, Workshops]


---


## Motivação

Este estudo tem como objetivo a definição da padronização da exibição de dashboards como o intuído de comunicar melhor através da imagens, definir um padrão de especificação de histórias a serem implementadas e definir conceitualmente os elementos que são manipulados. 

## Definição de Design Geral

A melhor definição para um design para a criação de dashboards é a simplicidade, apresentação suave e sem distrações.

### Apresentação de números

Conceitualmente os números podem carregar com si atributos como:
- Divisor [Milhares, Milhões, Bilhões]
- Escala [R$, Hectar, Unidade, Percentual, etc..]

A utilização da exibição das informações dos números ajuda na compreensão. Demostração clara de como simplificar a exibição das informações

Representações de BigNumbers precisa apresentar as informações de forma reduzida e com as devidas escalas. Um exemplo de um valor em bihões R$ 1.000.000.000, este valor não demostra clareza de qual valor estamos apresentando e neste caso podemos simplificar utilizando um divisor R$ 1 Bi

Exemplo do tratamento do eixo Y:

![Eixo com muitas informações](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-exibicao-dados/ruimexibição.png)
Neste caso as informações carrega muitas informação a escala esta repetido

![Eixo simplificado](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-exibicao-dados/boaexibicao.png)
Neste segundo exemplo o eixo Y fica simplificado e mais resumido

### Apresentação de cores

A utilização de cores pode gerar entendimentos errados ou mesmo perda de informação quando imprimimos um gráfico em preto e branco.

![Eixo simplificado](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-exibicao-dados/boaexibicao.png)
O mesmo gráfico utilizado anteriormente utiliza cores que não representam nada, porem a utilizar a cor vermelho gera um destaque de informação desnecessário.

![Eixo simplificado](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-exibicao-dados/cores.png)
Esta forma apresenta apenas tons de cinza e isso permite que o gráfico seja impresso e não perca informações, mesmo porque a utilização das cores pode gerar um interpretação errada dos dados.

### Tipos de gráficos

Gráfico de Barras: O gráfico de barras (ou de colunas) é utilizado em geral para representar dados de uma tabela de frequências associadas a uma variável qualitativa. Nesse tipo de gráfico, cada barra retangular representa a frequência ou a frequência relativa da respectiva opção da variável.

Gráfico de Linhas: O gráfico de linhas (ou de segmentos) é utilizado, em geral, para representar a evolução dos valores de uma variável no decorrer do tempo.

Histograma: As frequências absolutas e as relativas de dados agrupados em intervalos de classes podem ser representadas por meio de um tipo de gráfico denominado histograma, o qual é composto de retângulos justapostos cujas bases são apoiadas em um eixo horizontal.

Sparkline: Pequeno gráfico numa célula de folha de cálculo que fornece uma representação visual de dados. Pode utilizar Gráficos Sparkline para mostrar tendências numa série de valores, tais como aumentos ou diminuições sazonais e ciclos econômicos, ou para realçar valores máximos e mínimos.

## História de usuário

Para gerar uma história de usuário para representar uma necessidade de um usuário que necessita de uma informação, precisamos, inicialmente, definir alguns conceitos:
- Cliente: Quem irá consumir o dashboard;
- Dataset: Conjunto de dados que serão selecionados;
- Atributos: Atributos que serão exibidos primários ou calculados;
- Regras: Definição de comportamento dos dados através de filtro;
- Eventos: lista de eventos ou CTA que o indicador deve responder;

A criação da história deverá seguir a seguinte lei de formação
Ação - Atributos - Dataset - Cliente - Regras - Eventos;
Na prática:
Desenvolver visualização de dados[Ação] dos valores dispensados por Estado[Atributos] do conjunto de dados de dispensações[Dataset] para o gestor da farmácia popular[Cliente] apenas os dados dos estados do nordeste[Regras] para gerar uma lista de itens com divergências para realizar a notificação[Eventos]

##Fluxo de criação de indicadores

A forma mais indicada para a criação de um fluxo é:
- Criar a história de usuário
- Encontrar a melhor forma de exibição dos dados [Barra, LInhas, Histograma, SparkLine e etc..]
- Definir a lista de Call to action que o indicador ou dashboard deve responder

##Interface

Através da análise das necessidades gerais do BNAFAR um bom exemplo de dashboard é:
![Eixo simplificado](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-exibicao-dados/boadiagramacaodash.png)

Este tipo de exibição em sparkline consegue gerar em uma linha contendo todas as informações necessárias para a análise e tomada de decisão, assim como apresentar os valores que estão com possíveis divergências.
