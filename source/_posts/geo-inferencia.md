---
title: Inteligência Geográfica
date: 2019-07-23 10:05:04
toc: true
sidebar:
    left:
        sticky: true
widgets:
    -
        type: toc
        position: left

category:
# - [Estudo]
# - [Orçamento, Painel Orçamentário]
# - [Orçamento, PLOA automatizada]
 - [Preditivo, Levantamento Epidemiológico]
 - [Preditivo, Previsão de compras]
 - [Preditivo, Tecnologias Aplicadas]
 - [Farmácia Popular, Monitoramento]
# - [Farmácia Popular, Notificação]
# - [Farmácia Popular, Ressarcimento]
# - [Capacitação, Cursos]
# - [Capacitação, Eventos]
# - [Capacitação, Workshops]

tags:
---

# 

Nos últimos anos do século XX e notadamente no século XXI a gestão pública tem recebido a 
contribuição do geoprocessamento, que potencializou a capacidade dos gestores. Consiste 
em um conjunto de tecnologias de informação que indicam onde ocorrem os eventos em foco. 
Não se resume em apenas indicar locais, o termo "análise geoespacial" ou "análise espacial" 
responde perguntas como: o que é? Qual é o padrão? Aonde ir? O que mudou? Por que ocorre?, etc.   

Ter o conhecimento sobre os locais onde ocorrem os eventos é ter poder e controle sobre o território,
com elevadas chances de se atingir os resultados planejados. 

Tendo em vista o propósito de automatizar processos de informação no Departamento de Assistência 
Farmacêutica (DAF), os padrões de custos, distribuição, consumo, carências, eficiência na dispensação, 
etc., pertinentes à distribuição e consumo de medicamentos, tem a contribuição de elevada 
performance com o uso da inteligência geográfica. 



## Análise Espacial

Os eventos de saúde estão fortemente associados a aspectos regionais. As dinâmicas
socioeconômicas, ambientais, políticas e culturais geram particularidades que se 
expressam em perfis epidemiológicos distintos de uma área geográfica para outra.

As análises espaciais em saúde, em essência, têm o objetivo de caracterizar áreas pelos seus 
perfis epidemiológicos, por meio de inferências estatísticas e geoestatísticas para encontrar 
padrões que expliquem as fragilidades ou forças antrópicas e/ou naturais que afetam as 
populações restritas em determinadas fronteiras políticas como nos municípios, vilas, 
setores censitários, etc., ou em populações isoladas por exemplo, em ilhas ou aquelas 
separadas por cordilheiras.

## As Bases de Dados Geográficos

O desenvolvimento de estudos epidemiológicos se deve à clínica, que produz milhões 
de registros de pacientes, reunidos em bases de dados. A leitura destes dados 
favorece o entendimento e o plenejamento para controle de surtos e epidemias, notadamente pela 
ação da vigilância epidemiológica. Se as bases de dados foram fundamentais para esta evolução, 
mais ainda são as bases de dados geográficos, que se compõem de cartografias, imagens orbitais, 
registros numéricos e textuais. Este conjunto de fontes compõem cenários e possibilidades de se 
compreender padrões que aproximam significativamnete o observador ao mundo real, levando em conta 
variáveis geoespaciais até então não percebidas com precisão. 

## A Espacialização dos Dados de Dispensação de Medicamentos

A crescente prevalência de doenças degenerativas como 
hipertensão e diabetes têm levado o Ministério da Saúde a planejar métricas de dispensação 
de medicamentos a partir de bases de dados HORUS, SIM, AQTFP, CNES e AIH, 
com o objetivo de ampliar o acesso da população brasileira aos medicamentos e promover o seu uso 
racional. Dificuldades da dispensação agravam os problemas de saúde e aumentam os gastos 
com tratamentos. Neste sentido foi criado o Programa Farmácia Popular no Brasil, com redução de
90% do preço dos medicamentos, em rede própria e rede privada. Posteriormente o programa Saúde 
Não Tem Preço, com subvenção de 100%. 

Contribuindo com os estudos já realizados no Brasil a respeito da dificuldade de acesso dos indivíduos
aos locais de dispensação é proposta a análise espacial, que leve em conta a dificuldade de acesso
do indivíduo à farmácia (locais de dispensação), levando em consideração a localização do ponto de 
dispensação de medicamento; a localização do indivíduo que necessita do medicamento; a distância a 
ser percorrida pelo indivíduo até chegar na farmácia, pelas vias de acesso; a declividade entre o 
ponto incial e final do trajeto; a renda média dos habitantes por setores censitários.   

A farmácias foram localizadas no espaço geográfico pelo método de geocoding, a partir dos seus 
 endereços (imagem 1).

### Imagem 1. Distribuição espacial das farmácias 

![pacientes_farmacias](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-geo-inferencia/farmacias.jpeg)

### Imagem 2. Áreas de influência de 800 metros em torno das farmácias 

Áreas de influência de 800 metros em torno das farmácias

![pacientes_farmacias](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-geo-inferencia/area_influencia_800m.jpeg)



### Imagem 3. Áreas de influência de 1000 metros em torno das farmácias

![pacientes_farmacias](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-geo-inferencia/area_influencia_1000m.jpeg)


### Imagem 4. Áreas de influência de 1250 metros em torno das farmácias

Áreas de influência de 1250 metros em torno das farmácias

![pacientes_farmacias](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-geo-inferencia/area_influencia_1250m.jpeg)


Os pacientes que necessitam dos medicamentos foram geolocalizados também pelo método de geocoding
dos seus endereços (imagem 2).

### Imagem 4. A geolocalização dos consumidores de medicamentos e das farmácias

![pacientes_farmacias](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-geo-inferencia/pacientes.jpeg)



 
O conjunto de dados geolocalizados formado pelas farmácias, suas áreas de influência e pelos 
pacientes

### Imagem 5. Farmácias, pacientes e áreas de influência

![pacientes_farmacias](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-geo-inferencia/pacientes_farmacias_areas_influencia.jpeg)



As distâncias lineares entre os pacientes e as farmácias é calculado e indicado pelas linhas 
vermelhas, com a indicação da distância em metros.

### Imagem 6. As distâncias lineares entre os pacientes e as farmácias

![pacientes_farmacias](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-geo-inferencia/distancias_paciente_farmacia.jpeg)



Curvas de nível e declividade

### Imagem 7. A declividade no deslocamento do paciente até a farmácia

![pacientes_farmacias](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-geo-inferencia/curvas_de_nivel.jpeg)



Vias públicas

### Imagem 8. O deslocamento dos pacientes pelas vias públicas até as farmácias

![pacientes_farmacias](https://gitlab.com/gaesi/msdaf/wiki/raw/master/themes/icarus/source/images/post-geo-inferencia/vias_publicas.jpeg)








